//
//  ViewController.swift
//  SmartSip11
//
//  Created by PENA on 11/9/2559 BE.
//  Copyright © 2559 PENA. All rights reserved.
//

import UIKit
import CoreLocation
import FileBrowser
import MapKit


class ViewController: UIViewController ,CLLocationManagerDelegate, MKMapViewDelegate{
    var locationManager: CLLocationManager = CLLocationManager()
    var startLocation: CLLocation!
    var array = [String]()
    var docController:UIDocumentInteractionController!
    
    @IBOutlet weak var maps: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        maps.delegate = self
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        //array.append(UserDefaults.standard.value(forKey: "info")! as! String)
        //array = UserDefaults.standard.value(forKey: "info") as! [String]
       
  
    }
    
    @IBAction func openFileBtn(_ sender: Any) {
        let file = FileBrowser()
        present(file, animated: true, completion: nil)
    }
    
    @IBOutlet weak var lab_status: UILabel!
    
    @IBAction func open_btn(_ sender: Any) {
      
    }
    @IBAction func fileIn(_ sender: Any) {
        let file = "file.txt" //this is the file. we will write to and read from it
        
        let text = String(describing: UserDefaults.standard.value(forKey: "info"))   //just a text
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let path = dir.appendingPathComponent(file)
            
            //writing
            do {
                try text.write(to: path, atomically: false, encoding: String.Encoding.utf8)
            }
            catch {/* error handling here */}
            
            //reading
            do {
                let text2 = try String(contentsOf: path, encoding: String.Encoding.utf8)
                print("ssss\(text2)")
            }
            catch {/* error handling here */}
        }
    }
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0]
        let lat = String(userLocation.coordinate.latitude)
        let lng = String(userLocation.coordinate.longitude)
        let time = String(describing: userLocation.timestamp)
        let altitude   = String(userLocation.altitude)
        let speed   = String(userLocation.speed)
        
        let newYorkLocation = CLLocationCoordinate2DMake(userLocation.coordinate.latitude, userLocation.coordinate.longitude)
        // Drop a pin
        let dropPin = MKPointAnnotation()
        dropPin.coordinate = newYorkLocation
        dropPin.title = "New York City"
        maps.addAnnotation(dropPin)
        
       
        
        //print("\(lat) \(lng) \(time) \(altitude) \(speed)")
        let str = String("\(lat) \(lng) \(time) \(altitude) \(speed)")
        lab_status.text = String(time)
        array.append(str!)
        UserDefaults.standard.setValue(array, forKey: "info")
        
        let a = UserDefaults.standard.value(forKey: "info")!
        
        print("---- \((a as AnyObject).count)")
        print("---- \(UserDefaults.standard.value(forKey: "info")!)")
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

